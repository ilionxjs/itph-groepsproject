package com.ilionx.opleiding;

public enum OpleidingsNiveau {
    MBO, HBO, WO
}
