package com.ilionx.opleiding;

import com.ilionx.opleiding.Beoordeling;
import com.ilionx.opleiding.Opleidingsinstituut;

import java.util.List;

public class Studie {

    private int duur;
    private double kosten;
    private String naam;
    private List<Opleidingsinstituut> opleidingsinstituten;
    private List<Beoordeling> beoordelingen;

    public void setDuur(int duur) {
        this.duur = duur;
    }

    public void setKosten(double kosten) {
        this.kosten = kosten;
    }

    public void setBeoordelingen(List<Beoordeling> beoordelingen) {
        this.beoordelingen = beoordelingen;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public void setOpleidingsinstituten(List<Opleidingsinstituut> opleidingsinstituten) {
        this.opleidingsinstituten = opleidingsinstituten;
    }

    public double getKosten() {
        return kosten;
    }

    public int getDuur() {
        return duur;
    }

    public List<Beoordeling> getBeoordelingen() {
        return beoordelingen;
    }

    public String getNaam() {
        return naam;
    }

    public List<Opleidingsinstituut> getOpleidingsinstituten() {
        return opleidingsinstituten;
    }

}
