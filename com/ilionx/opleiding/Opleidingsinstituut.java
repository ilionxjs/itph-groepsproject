package com.ilionx.opleiding;

import com.ilionx.account.Opleidingsaccount;

import java.util.List;

public class Opleidingsinstituut {

    private String naam;
    private String intro;
    private String plaats;
    private String url;
    private List<OpleidingsNiveau> opleidingsniveau;
    private Opleidingsaccount opleidingsaccount;
    private List<Studie> studies;
    private List<Beoordeling> beoordelingen;

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public void setBeoordelingen(List<Beoordeling> beoordelingen) {
        this.beoordelingen = beoordelingen;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public void setOpleidingsaccount(Opleidingsaccount opleidingsaccount) {
        this.opleidingsaccount = opleidingsaccount;
    }

    public void setOpleidingsniveau(List<OpleidingsNiveau> opleidingsniveau) {
        this.opleidingsniveau = opleidingsniveau;
    }

    public void setStudies(List<Studie> studies) {
        this.studies = studies;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNaam() {
        return naam;
    }

    public String getIntro() {
        return intro;
    }

    public List<Beoordeling> getBeoordelingen() {
        return beoordelingen;
    }

    public List<OpleidingsNiveau> getOpleidingsniveau() {
        return opleidingsniveau;
    }

    public List<Studie> getStudies() {
        return studies;
    }

    public Opleidingsaccount getOpleidingsaccount() {
        return opleidingsaccount;
    }

    public String getPlaats() {
        return plaats;
    }

    public String getUrl() {
        return url;
    }

}
