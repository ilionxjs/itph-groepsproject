package com.ilionx.opleiding;

public class Beoordeling {

    private double rating;
    private Opleidingsinstituut opleidingsinstituut;
    private Studie studie;
    private String geschrevenReview;
    private String responseOpleidingsInstituut;

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getRating() {
        return rating;
    }

    public void setOpleidingsinstituut(Opleidingsinstituut opleidingsinstituut) {
        this.opleidingsinstituut = opleidingsinstituut;
    }

    public Opleidingsinstituut getOpleidingsinstituut() {
        return opleidingsinstituut;
    }

    public void setStudie(Studie studie) {
        this.studie = studie;
    }

    public Studie getStudie() {
        return studie;
    }

    public void setGeschrevenReview(String geschrevenReview) {
        this.geschrevenReview = geschrevenReview;
    }

    public String getGeschrevenReview() {
        return geschrevenReview;
    }

    public void setResponseOpleidingsInstituut(String responseOpleidingsInstituut) {
        this.responseOpleidingsInstituut = responseOpleidingsInstituut;
    }

    public String getResponseOpleidingsInstituut() {
        return responseOpleidingsInstituut;
    }
}
